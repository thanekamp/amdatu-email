/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.aws;

import java.util.ArrayList;
import java.util.Dictionary;

import org.amdatu.email.EmailException;
import org.amdatu.email.EmailService;
import org.amdatu.email.Message;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

public class AWSEmailService implements EmailService, ManagedService {
    private volatile LogService m_logService;
    private volatile String m_awsKey;
    private volatile String m_awsSecret;
    private volatile Region m_region;

    @Override
    public void send(Message message) {
        com.amazonaws.services.simpleemail.model.Message sesMessage =
            new com.amazonaws.services.simpleemail.model.Message()
                .withSubject(new Content(message.getSubject()))
                .withBody(
                    new Body().withHtml(new Content(message.getHtmlBody()))
                        .withText(new Content(message.getTextBody())));

        SendEmailRequest request =
            new SendEmailRequest()
                .withSource(message.getFormattedFrom())
                .withDestination(new Destination(new ArrayList<>(message.getRecipients())))
                .withMessage(
                    sesMessage);

        AmazonSimpleEmailServiceClient client =
            new AmazonSimpleEmailServiceClient(
                new BasicAWSCredentials(
                    m_awsKey,
                    m_awsSecret));

        client.setRegion(m_region);

        try {
            client.sendEmail(request);
        }
        catch (AmazonClientException ex) {
            m_logService.log(LogService.LOG_ERROR, "Error sending email message", ex);
            throw new EmailException(message, ex);
        }

    }

    @Override
    public void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
        String awsKey = (String) properties.get("aws-key");
        String awsSecret = (String) properties.get("aws-secret");
        String awsRegion = (String) properties.get("aws-region");

        if (awsKey == null) {
            throw new ConfigurationException("aws-key", "Required configuration property aws-key missing");
        }

        if (awsSecret == null) {
            throw new ConfigurationException("aws-secret", "Required configuration property aws-secret missing");
        }

        if (awsRegion == null) {
            m_region = Region.getRegion(Regions.DEFAULT_REGION);
        } else {
            Region region = keyToRegion(awsRegion);
            if (region != null) {
                m_region = region;
            } else {
                throw new ConfigurationException("aws-region", "Could not create region from string '" + awsRegion
                    + "'");
            }
        }

        m_awsKey = awsKey;
        m_awsSecret = awsSecret;
    }
    
    private Region keyToRegion(String key) {
        try {
            return Region.getRegion(Regions.fromName(key));
        }
        catch (IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }
}
