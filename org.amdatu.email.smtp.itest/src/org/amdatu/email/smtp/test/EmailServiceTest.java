/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.smtp.test;

import junit.framework.TestCase;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import org.amdatu.email.EmailService;
import org.amdatu.email.Message;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;

/**
 * Run this test to verify that connectivity to SMTP Mail Provider works. A mock SMTP
 * server is started as part of this integration test.
 */
public class EmailServiceTest extends TestCase {
	
    private static final String smtpHost = "localhost"; // Replace with your SMTP server
    private static final String smtpUser = "user"; // Replace with valid user name
    private static final String smtpPassword = "password"; // Replace with valid password
    private static final Integer smtpPort = 2500; // 465, 25, or custom port number
    private static final Boolean smtpSSL = true; // true or false

    private volatile EmailService m_emailService;
    
    private SimpleSmtpServer smtpServer;

    @Override
    public void setUp() {
        smtpServer = SimpleSmtpServer.start(smtpPort);

        if (smtpHost == null || smtpUser == null || smtpPassword == null) {
            fail("Please provide valid smtpHost, smtpUser, and smtpPassword before running this test");
        }
        
        configure(this).add(createConfiguration("org.amdatu.email.smtp")
        	.set("smtp-host", smtpHost)
        	.set("smtp-user", smtpUser)
        	.set("smtp-password", smtpPassword)
        	.set("smtp-port", smtpPort.toString())
        	.set("smtp-use-ssl", smtpSSL.toString()))
        .add(createServiceDependency().setService(EmailService.class).setRequired(true)).apply();
    }
    
    @Override
    public void tearDown() {
        smtpServer.stop();
        smtpServer = null;
        cleanUp(this);
    }

    public void testSuccessfulEmail() {
        String testBody = "Amdatu Mail Service Test";
        String testSubject = "Test Message";
        String from = "test@amdatu.org";
        String fromFriendlyName = "Amdatu SMTP Integration test";
        m_emailService.send(Message.Builder.create().recipient("someone@example.com").textBody(testBody).subject(testSubject)
            .from(from, fromFriendlyName).build());

        assertEquals(1, smtpServer.getReceivedEmailSize());
        SmtpMessage email = (SmtpMessage) smtpServer.getReceivedEmail().next();
        assertEquals(testSubject, email.getHeaderValue("Subject"));
        assertTrue(email.getBody().contains(testBody));
        assertEquals(fromFriendlyName + " <" + from + ">", email.getHeaderValue("From"));
        // Make sure message is sent as plain email
        assertFalse(email.getBody().contains("text/html"));
    }
    
    public void testSuccessfulHtmlEmail() {
        String testBody = "<html><p>Amdatu Mail Service Test</p></html>";
        String testSubject = "Test HTML Message";
        String from = "test@amdatu.org";
        String fromFriendlyName = "Amdatu SMTP Integration test";
        m_emailService.send(Message.Builder.create().recipient("someone@example.com").htmlBody(testBody).subject(testSubject)
            .from(from, fromFriendlyName).build());

        assertEquals(1, smtpServer.getReceivedEmailSize());
        SmtpMessage email = (SmtpMessage) smtpServer.getReceivedEmail().next();
        assertEquals(testSubject, email.getHeaderValue("Subject"));
        assertTrue(email.getBody().contains(testBody));
        assertEquals(fromFriendlyName + " <" + from + ">", email.getHeaderValue("From"));
        // Make sure message is sent as multipart html email
        assertTrue(email.getBody().contains("text/html"));
    }
    
}