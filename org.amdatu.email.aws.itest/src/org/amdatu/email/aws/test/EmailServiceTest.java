/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.aws.test;

import junit.framework.TestCase;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import org.amdatu.email.EmailService;
import org.amdatu.email.Message;

/**
 * Run this test to verify that connectivity to Amazon works. Before running this this,
 * you must supply valid credentials. The associated Amazon account must have rights to
 * send email through SES. Also make sure the correct region is configured for the given
 * credentials.
 * 
 * !!! Do not check in credentials after running this test !!!
 */
public class EmailServiceTest extends TestCase {
	
    private static final String awsKey = null;
    private static final String awsSecret = null;
    // Ensure the region is correct, wrong region leads to '400 - Email address is not verified'
    private static final String awsRegion = null;

	private volatile EmailService m_emailService;

    @Override
    public void setUp() throws Exception {
        if(awsKey == null || awsSecret == null) {
            System.out.println("Setup failed! Please provide valid awsKey and awsSecret before running this test");
            return;
        }
        
        configure(this)
        	.add(createConfiguration("org.amdatu.email.aws")
        		.set("aws-key", awsKey)
        		.set("aws-secret", awsSecret)
        		.set("aws-region", awsRegion))
        	.add(createServiceDependency().setService(EmailService.class).setRequired(true)).apply();
        
        super.setUp();
    }

    public void testSuccessfulEmail() {
    	if(awsKey == null || awsSecret == null) {
            System.out.println("Skipped test! Please provide valid awsKey and awsSecret before running this test");
            return;
        }
        m_emailService.send(Message.Builder.create().recipient("success@simulator.amazonses.com").htmlBody("test")            
        		.subject("test").from("test@amdatu.com", "Amdatu AWS integration test").build());
    }
    
}