/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.smtp;

import java.util.Dictionary;

import org.amdatu.email.EmailService;
import org.amdatu.email.Message;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

public class SmtpEmailService implements EmailService, ManagedService {
    private final static int DEFAULT_SMTP_PORT = 25;
    private volatile LogService m_logService;
    private volatile String m_smtpHost;
    private volatile int m_smtpPort;
    private volatile boolean m_smtpUseSSL = false;
    private volatile String m_smtpUser;
    private volatile String m_smtpPassword;
    private volatile String m_smtpFromEmail;
    private volatile String m_smtpFromName;

    @Override
    public void send(Message message) {
        // Temporarily switch TCCL so JavaMail can resolve DataContentHandlers
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(javax.mail.Session.class.getClassLoader());

            Email email = null;
            if (message.getHtmlBody() != null) {
            	HtmlEmail htmlEmail = new HtmlEmail();
            	htmlEmail.setHtmlMsg(message.getHtmlBody());
            	if (message.getTextBody() != null) {
            		htmlEmail.setTextMsg(message.getTextBody());
            	}
                email = htmlEmail;
            } else {
                email = new SimpleEmail();
                email.setMsg(message.getTextBody());
            }
            email.setHostName(m_smtpHost);
            email.setAuthenticator(new DefaultAuthenticator(m_smtpUser, m_smtpPassword));
            email.setSmtpPort(m_smtpPort);
            email.setSSL(m_smtpUseSSL);
            if (message.getFrom() != null) {
            	String fromFriendlyName = message.getFromFriendlyName();
            	if (fromFriendlyName == null) {
            		fromFriendlyName = m_smtpFromName;
            	}
            	email.setFrom(message.getFrom(), fromFriendlyName);
            } else {
            	email.setFrom(m_smtpFromEmail, m_smtpFromName);
            }
            for (String recipient : message.getRecipients()) {
                email.addTo(recipient);
            }
            email.setSubject(message.getSubject());

            email.send();
        }
        catch (EmailException ex) {
            m_logService.log(LogService.LOG_ERROR, "Error sending email message", ex);
            throw new org.amdatu.email.EmailException(message, ex);
        }
        finally {
            // Switch back
            Thread.currentThread().setContextClassLoader(cl);
        }
    }

    @Override
    public void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
		if (properties != null) {
			String smtpHost = (String) properties.get("smtp-host");
			String smtpPort = (String) properties.get("smtp-port");
			String smtpUseSSL = (String) properties.get("smtp-use-ssl");
			String smtpUser = (String) properties.get("smtp-user");
			String smtpPassword = (String) properties.get("smtp-password");
			String smtpFromEmail = (String) properties.get("smtp-from-email");
			String smtpFromName = (String) properties.get("smtp-from-name");

			if (smtpHost == null) {
				throw new ConfigurationException("smtp-host", "Required configuration property smtp-host missing");
			}

			if (smtpUser == null) {
				throw new ConfigurationException("smtp-user", "Required configuration property smtp-user missing");
			}

			if (smtpPort == null) {
				m_smtpPort = DEFAULT_SMTP_PORT;
			} else {
				try {
					m_smtpPort = Integer.parseInt(smtpPort);
				} catch (NumberFormatException nfe) {
					throw new ConfigurationException(
							"smtp-port", "Optional configuration property smtp-port has illegal value. " 
									+ "Value should be 25, 465, or custom port number.");
				}
			}

			if (smtpPassword == null) {
				throw new ConfigurationException("smtp-password", "Required configuration property smtp-password missing");
			}

			if (smtpFromEmail == null) {
				m_smtpFromEmail = "noreply@amdatu.org";
			} else {
				m_smtpFromEmail = smtpFromEmail;
			}

			if (smtpFromName == null) {
				m_smtpFromName = "Amdatu Mail Service";
			} else {
				m_smtpFromName = smtpFromName;
			}

			m_smtpHost = smtpHost;
			m_smtpUseSSL = Boolean.getBoolean(smtpUseSSL);
			m_smtpUser = smtpUser;
			m_smtpPassword = smtpPassword;
		}
    }
}