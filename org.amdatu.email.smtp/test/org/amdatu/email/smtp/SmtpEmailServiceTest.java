/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.smtp;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.email.smtp.SmtpEmailService;
import org.junit.Before;
import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;

public class SmtpEmailServiceTest {
	private SmtpEmailService m_smtpEmailService;
    
    @Before
    public void setup() {
        m_smtpEmailService = new SmtpEmailService();
    }

    @Test
    public void testCorrectConfig() throws ConfigurationException {
        m_smtpEmailService.updated(getProperties());
    }
    
    @Test
    public void testCorrectConfigWithOptionalAttributes() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.put("smtp-port", "465");
        properties.put("smtp-use-ssl", "true");
        properties.put("smtp-from-name", "Test Tickle");
        properties.put("smtp-from-email", "test@amdatu.org");
        m_smtpEmailService.updated(properties);
    }
 
    @Test(expected = ConfigurationException.class)
    public void testConfigWithMissingPassword() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.remove("smtp-password");
        m_smtpEmailService.updated(properties);
    }
 
    @Test(expected = ConfigurationException.class)
    public void testConfigWithMissingUser() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.remove("smtp-user");
        m_smtpEmailService.updated(properties);
    }

    @Test(expected = ConfigurationException.class)
    public void testConfigWithMissingHost() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.remove("smtp-host");
        m_smtpEmailService.updated(properties);
    }
    
    @Test(expected = ConfigurationException.class)
    public void testConfigWithIncorrectPort() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.put("smtp-port", "abc");
        m_smtpEmailService.updated(properties);
    }

    private Dictionary<String, String> getProperties() {
        Dictionary<String, String> props = new Hashtable<>();
        props.put("smtp-host", "host");
        props.put("smtp-user", "user");
        props.put("smtp-password", "password");
        return props;
    }
}